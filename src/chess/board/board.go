package board

import (
	. "chess/common_types"
	. "chess/util"
	"strconv"
	"bytes"
)

type board struct {
	fields Fields
	castlingState CastlingStateInfo
	kingTaken bool
}

func NewBoard() *board {
	result := new(board)
	result.castlingState = NewCastlingState()
	result.kingTaken = false
	return result
}

func (b *board) WithCastling(castlingState CastlingStateInfo) BoardInfo{
	result := new(board)
	result.castlingState = castlingState
	result.fields = b.fields
	result.kingTaken = b.kingTaken
	return result
}

func (b *board) WithNewPiece(p Piece, row, col int) *board {
	result := new(board)
	result.castlingState = b.castlingState
	result.fields = b.fields
	result.fields[row][col] = p
	result.kingTaken = b.kingTaken
	return result
}

func (b *board) WithMovedPiece(move Move) BoardInfo {
	result := new(board)
	result.castlingState = b.castlingState.AfterMoveOf(move.GetPiece(), move.GetFromCol())
	result.fields = b.fields

	k := b.GetPiece(move.GetToRow(), move.GetToCol())
	takesKing := k != nil && k.GetType() == KingType && move.GetPiece().GetType() != KingType
	result.kingTaken = takesKing

	move.Execute(&result.fields)
	return result
}

func (b *board) WeightScore() (result int64) {
	for row := range b.fields {
		for col := range b.fields[row] {
			piece := b.GetPiece(row, col)
			if piece != nil {
				result += piece.GetWeightScore()
			}
		}
	}

	return result
}

func (b *board) WeightPlusCoverageScore() (result int64) {
	for row := range b.fields {
		for col := range b.fields[row] {
			piece := b.GetPiece(row, col)
			if piece != nil {
				c := piece.Coverage(b, row, col)
				w := piece.GetWeightScore() // signed
				result += w * (10000 + int64(c))
			}
		}
	}

	return
}

func (b *board) Score() int64 {
	return b.WeightPlusCoverageScore()
}

func (b *board) KingTaken() bool {
	return b.kingTaken
}

func (b *board) GetPiece(row, col int) Piece {
	return b.fields[row][col]
}

func (b *board) IsFree(row, col int) bool {
	return b.fields[row][col] == nil
}

func (b *board) AreFreeRows(rows Interval, col int) bool {
	for row := rows.First; row <= rows.Last; row++ {
		if b.fields[row][col] != nil {
			return false
		}
	}
	return true
}

func (b *board) AreFreeCols(row int, cols Interval) bool {
	for col := cols.First; col <= cols.Last; col++ {
		if b.fields[row][col] != nil {
			return false
		}
	}
	return true
}

func (b *board) IsPieceOfColor(row, col int, c Color) bool {
	p := b.GetPiece(row, col)
	return p != nil && p.GetColor() == c
}

func (b *board) IsBlackPiece(row, col int) bool {
	return b.IsPieceOfColor(row, col, Black)
}

func (b *board) IsWhitePiece(row, col int) bool {
	return b.IsPieceOfColor(row, col, White)
}

func (b *board) AvailableStraitMoves(color Color) []Move {
	result := make([]Move, 0, 128)

	for row := range b.fields {
		for col := range b.fields[row] {
			piece := b.GetPiece(row, col)
			if piece != nil && piece.GetColor() == color {
				result = append(result, piece.AvailableStraitMoves(b, row, col)...)
			}
		}
	}

	return result
}

func (b *board) AvailableMoves(color Color) []Move {
	result := make([]Move, 0, 128)

	for row := range b.fields {
		for col := range b.fields[row] {
			piece := b.GetPiece(row, col)
			if piece != nil && piece.GetColor() == color {
				result = append(result, piece.AvailableMoves(b, row, col)...)
			}
		}
	}

	return result
}

func (b *board) BooleanCoverage(color Color) Coverage {
	var result Coverage

	for _, m := range b.AvailableStraitMoves(color) {
		result[m.GetToRow()][m.GetToCol()] = true
	}

	return result
}

func (b *board) CoverageSum(color Color) int {
	possible := b.BooleanCoverage(color)
	result := 0

	for _, row := range possible {
		for _, item := range row {
			if item {
				result++
			}
		}
	}

	return result
}

func (b *board) GetCastlingState() CastlingStateInfo {
	return b.castlingState
}

func (b *board) String() string {
	var buffer bytes.Buffer

	for row := 7; row >= 0; row-- {
		for col := 0; col < 8; col++ {
			if col == 0 {
				buffer.WriteString(strconv.Itoa(row + 1))
			}

			p := b.GetPiece(row, col)


			if p == nil {
				buffer.WriteString("-")
			} else {
				buffer.WriteString(p.GetSymbol())
			}

			if col == 7 && row != 0 {
				buffer.WriteString("\n")
			}
		}
	}

	return buffer.String()
}
