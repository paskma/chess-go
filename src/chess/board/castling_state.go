package board

import (
	. "chess/common_types"
	"bytes"
	"strconv"
)

type CastlingState struct {
	whiteKingSideAvailable,
	whiteQueenSideAvailable,
	blackKingSideAvailable,
	blackQueenSideAvailable bool
}

func NewCastlingState() *CastlingState {
	return &CastlingState{true, true, true, true}
}

func NewCastlingStateParams(whiteKingSideAvailable,
	whiteQueenSideAvailable,
	blackKingSideAvailable,
	blackQueenSideAvailable bool) *CastlingState {

	return &CastlingState{
		whiteKingSideAvailable,
		whiteQueenSideAvailable,
		blackKingSideAvailable,
		blackQueenSideAvailable}
}

func DisabledCastling() *CastlingState {
	return &CastlingState{false, false, false, false}
}

func (c *CastlingState)WithDisabled(disableWhiteKingSide, disableWhiteQueenSide,
	disableBlackKingSide, disableBlackQueenSide bool) *CastlingState {
	return &CastlingState{
		c.whiteKingSideAvailable && !disableWhiteKingSide,
		c.whiteQueenSideAvailable && !disableWhiteQueenSide,
		c.blackKingSideAvailable && !disableBlackKingSide,
		c.blackQueenSideAvailable && !disableBlackQueenSide}
}

func (c *CastlingState)IsWhiteKingSideAvailable() bool {
	return c.whiteKingSideAvailable
}

func (c *CastlingState)IsWhiteQueenSideAvailable() bool {
	return c.whiteQueenSideAvailable
}

func (c *CastlingState)IsBlackKingSideAvailable() bool {
	return c.blackKingSideAvailable
}

func (c *CastlingState)IsBlackQueenSideAvailable() bool {
	return c.blackQueenSideAvailable
}

func (s *CastlingState)AfterMoveOf(p Piece, sourceCol int) CastlingStateInfo {
	if (p.GetType() == KingType) {
		if p.IsWhite() {
			return s.WithDisabled(true, true, false, false)
		} else {
			return s.WithDisabled(false, false, true, true)
		}
	} else if (p.GetType() == RookType) {
		if (p.IsWhite()) {
			if sourceCol == 0 {
				return s.WithDisabled(false, true, false, false)
			} else if sourceCol == 7 {
				return s.WithDisabled(true, false, false, false)
			} else {
				return s
			}
		} else {
			if sourceCol == 0 {
				return s.WithDisabled(false, false, false, true)
			} else if sourceCol == 7 {
				return s.WithDisabled(false, false, true, false)
			}else {
				return s
			}
		}
	} else {
		return s
	}
}

const (
	Key_whiteKingSideCastling = "whiteKingSideCastling"
	Key_whiteQueenSideCastling = "whiteQueenSideCastling"
	Key_blackKingSideCastling = "blackKingSideCastling"
	Key_blackQueenSideCastling = "blackQueenSideCastling"
)

type line struct {
	key string
	value bool
}

func (c *CastlingState) String() string {
	values := []line{
		line{Key_whiteKingSideCastling, c.IsWhiteKingSideAvailable()},
		line{Key_whiteQueenSideCastling, c.IsWhiteQueenSideAvailable()},
		line{Key_blackKingSideCastling, c.IsBlackKingSideAvailable()},
		line{Key_blackQueenSideCastling, c.IsBlackQueenSideAvailable()},
	}

	var buffer bytes.Buffer

	for _, item := range values {
		buffer.WriteString(item.key)
		buffer.WriteString("=")
		buffer.WriteString(strconv.FormatBool(item.value))
		buffer.WriteString("\n")
	}

	return buffer.String()
}
