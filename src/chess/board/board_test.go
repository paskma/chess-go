package board

import (
	"testing"
	. "chess/piece"
	. "chess/common_types"
	. "chess/move"
)


func TestImmutability(t *testing.T) {
	empty := NewBoard()
	withPawn := empty.WithNewPiece(NewPawn(Black), 1, 1)

	if !empty.IsFree(1, 1) { t.Error("It mutates.") }
	if withPawn.IsFree(1, 1) { t.Fail() }
}


func TestSimpleMove(t *testing.T) {
	pawn := NewPawn(White)
	board := NewBoard().WithNewPiece(pawn, 1, 1)
	moved := board.WithMovedPiece(NewSimpleMove(pawn, 1, 1, 2, 1))

	if !moved.IsFree(1, 1) { t.Fail() }
	if moved.GetPiece(2, 1) != pawn { t.Fail() }
}

func TestWeight(t *testing.T) {
	pawn := NewPawn(Black)
	board := NewBoard().WithNewPiece(pawn, 5, 5)

	if board.WeightScore() != -1 { t.Fail() }
}


func TestCoverageSum(t *testing.T) {
	rook := NewRook(Black)
	board := NewBoard().WithNewPiece(rook, 0, 0)

	if board.CoverageSum(White) != 0 { t.Error("No white pieces, expecting no coverage.") }
	if board.CoverageSum(Black) != 14 { t.Error("Rook in the corner.")}
}
