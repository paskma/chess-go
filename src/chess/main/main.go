package main

import (
	"fmt"
	"sort"
	"time"
	"os"
	"flag"
	"log"
	"io/ioutil"
	"runtime/pprof"
	. "chess/board_io"
	. "chess/algorithm"
	. "chess/common_types"
	. "chess/util"
)

var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to file")
var depth = flag.Int("d", 2, "depth")
var movesToPrint = flag.Int("m", 2, "moves to print")
var castlingFile = flag.String("c", "", "castling file")
var outputBoardFile = flag.String("o", "", "output board file")
var inputBoardFile = flag.String("i", "", "input board file")
var algorithmName = flag.String("s", "AlphaBeta", "solver (ParallelAlphaBeta, AlphaBeta, Minimax)")

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func createSolver(algorithm string) Solver {
	switch algorithm {
	case "Minimax":
		return new(Minimax)
	case "AlphaBeta":
		return new(Alphabeta)
	case "ParallelAlphaBeta":
		return new (ParallelAlphabeta)
	default:
		panic("Unknown algorithm " + algorithm)
	}
}

func main() {
	flag.Parse()
	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}

	if *inputBoardFile == "" {
		println("No input board file. Try -help.")
		return
	}

	if *castlingFile == "" {
		println("No castling file.")
		return
	}

	boardData, err := ioutil.ReadFile(*inputBoardFile)
	check(err)

	board := FromString(string(boardData))
	fmt.Println(board)

	castlingData, err := ioutil.ReadFile(*castlingFile)
	check(err)

	castlingState := CastlingStateFromString(string(castlingData))
	board = board.WithCastling(castlingState)

	solver := createSolver(*algorithmName)
	messages := make(map[string]interface {})
	start := time.Now()

	evaluatedMoves := solver.EvaluatedMoves(board, White, *depth, messages)
	elapsed := time.Since(start)
	sort.Sort(ByEvaluation(evaluatedMoves))

	for i := 0; i < Min(*movesToPrint, len(evaluatedMoves)); i++ {
		fmt.Println(evaluatedMoves[i])
	}

	bestMove := evaluatedMoves[0]
	bestBoard := board.WithMovedPiece(bestMove.Move)

	fmt.Println(messages["positionsEvaluated"].(int))
	fmt.Printf("elapsed %s\n", elapsed)
	fmt.Println(bestBoard.String())

	if *outputBoardFile != "" {
		ioutil.WriteFile(*outputBoardFile, []byte(bestBoard.String()), 0644)
	}

	fmt.Println("Done.")
}
