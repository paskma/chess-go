package algorithm

import (
	. "chess/common_types"
	. "chess/util"
	"math"
)

type Alphabeta struct {
}

func (m *Alphabeta) EvaluatedMoves(board BoardInfo, player Color, depth int, messages map[string]interface {}) []EvaluatedMove {
	result := make([]EvaluatedMove, 0, 128)
	interceptor := new(SimpleInterceptor)

	moves := board.AvailableMoves(player)
	for _, move := range moves {
		evaluatedMove := EvaluatedMove {
			move,
			m.evaluate(board.WithMovedPiece(move), player.Other(), depth,
				math.MinInt64,
				math.MaxInt64,
				interceptor),
		}
		result = append(result, evaluatedMove)
	}

	messages["positionsEvaluated"] = interceptor.PositionsEvaluated

	return result
}

func (m *Alphabeta)evaluate(board BoardInfo, player Color, depth int,
		alpha, beta int64, interceptor Interceptor) int64 {
	if depth == 0 || board.KingTaken() {
		interceptor.Evaluated()
		return board.Score()
	}

	moves := board.AvailableMoves(player)
	boards := make([]BoardInfo, 0, len(moves))
	for _, move := range moves {
		boards = append(boards, board.WithMovedPiece(move))
	}

	if len(boards) == 0 {
		// terminal point
		interceptor.Evaluated()
		return board.Score()
	}

	if player == White {
		resultAlpha := alpha
		for _, b := range boards {
			resultAlpha = Max64(resultAlpha, m.evaluate(b, Black, depth -1, resultAlpha, beta, interceptor))
			if beta <= resultAlpha {
				break
			}
		}
		return resultAlpha
	} else {
		resultBeta := beta
		for _, b := range boards {
			resultBeta = Min64(resultBeta, m.evaluate(b, White, depth - 1, alpha, resultBeta, interceptor))
			if resultBeta <= alpha {
				break;
			}
		}
		return resultBeta
	}
}
