package algorithm

type Interceptor interface {
	Evaluated()
}

type SimpleInterceptor struct {
	PositionsEvaluated int
}

func (si *SimpleInterceptor)Evaluated() {
	si.PositionsEvaluated++
}

func mergeInterceptors(a, b *SimpleInterceptor) *SimpleInterceptor {
	return &SimpleInterceptor{PositionsEvaluated : a.PositionsEvaluated + b.PositionsEvaluated}
}
