package algorithm

import (
	. "chess/common_types"
)

type EvaluatedMove struct {
	Move Move
	Evaluation int64
}

type ByEvaluation []EvaluatedMove

func (a ByEvaluation) Len() int           { return len(a) }
func (a ByEvaluation) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByEvaluation) Less(i, j int) bool { return a[i].Evaluation > a[j].Evaluation }
