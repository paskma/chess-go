package algorithm

import (
	"testing"
	"sort"
)

func TestImmutability(t *testing.T) {
	moves := []EvaluatedMove{{nil, 1},{nil,2}}
	sort.Sort(ByEvaluation(moves))
	if moves[0].Evaluation != 2 { t.Error("Best move shall be first.") }
}
