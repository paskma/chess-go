package algorithm

import (
	. "chess/common_types"
)

type Solver interface {
	EvaluatedMoves(board BoardInfo, player Color, depth int, messages map[string]interface {}) []EvaluatedMove
}
