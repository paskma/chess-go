package algorithm

import (
	. "chess/common_types"
	. "chess/util"
)

type Minimax struct {
}

func (m *Minimax) EvaluatedMoves(board BoardInfo, player Color, depth int, messages map[string]interface {}) []EvaluatedMove {
	result := make([]EvaluatedMove, 0, 128)
	interceptor := new(SimpleInterceptor)

	moves := board.AvailableMoves(player)
	for _, move := range moves {
		evaluatedMove := EvaluatedMove {
			move,
			m.evaluate(board.WithMovedPiece(move), player.Other(), depth, interceptor),
		}
		result = append(result, evaluatedMove)
	}

	messages["positionsEvaluated"] = interceptor.PositionsEvaluated

	return result
}

func (m *Minimax)evaluate(board BoardInfo, player Color, depth int, interceptor Interceptor) int64 {
	if depth == 0 || board.KingTaken() {
		interceptor.Evaluated()
		return board.Score()
	}

	moves := board.AvailableMoves(player)
	boards := make([]BoardInfo, 0, len(moves))
	for _, move := range moves {
		boards = append(boards, board.WithMovedPiece(move))
	}

	if len(boards) == 0 {
		// terminal point
		interceptor.Evaluated()
		return board.Score()
	}

	evaluations := make([]int64, 0, len(boards))
	for _, b := range boards {
		evaluation := m.evaluate(b, player.Other(), depth - 1, interceptor)
		evaluations = append(evaluations, evaluation)
	}

	if player == White {
		return MaxItem64(evaluations)
	} else {
		return MinItem64(evaluations)
	}
}
