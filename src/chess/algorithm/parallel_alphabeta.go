package algorithm

import (
	. "chess/common_types"
	"math"
	"runtime"
)

type ParallelAlphabeta struct {
	impl Alphabeta
}


type empty struct {}

type iterationResult struct {
	move EvaluatedMove
	interceptor *SimpleInterceptor
}

func (m *ParallelAlphabeta) EvaluatedMoves(board BoardInfo, player Color, depth int, messages map[string]interface {}) []EvaluatedMove {
	result := make([]EvaluatedMove, 0, 256)
	
	runtime.GOMAXPROCS(runtime.NumCPU())
	semaphores := make(chan empty, runtime.NumCPU())

	moves := board.AvailableMoves(player)
	iterationResults := make(chan iterationResult, len(moves))
	for _, move := range moves {
		go func(move Move) {
			semaphores <- empty{}
			interceptor := new(SimpleInterceptor)
			evaluatedMove := EvaluatedMove {
				move,
				m.impl.evaluate(board.WithMovedPiece(move), player.Other(), depth,
					math.MinInt64,
					math.MaxInt64,
					interceptor),
			}
			iterationResults <- iterationResult{evaluatedMove, interceptor}
			<- semaphores
		}(move)
	}

	accumulatedInterceptor := new(SimpleInterceptor)
	for i := 0; i < len(moves); i++ {
		oneIterationResult := <- iterationResults
		result = append(result, oneIterationResult.move)
		accumulatedInterceptor = mergeInterceptors(accumulatedInterceptor, oneIterationResult.interceptor)
	}

	messages["positionsEvaluated"] = accumulatedInterceptor.PositionsEvaluated

	return result
}
