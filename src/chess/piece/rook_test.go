package piece

import (
	"testing"
	. "chess/board"
	. "chess/common_types"
)


func TestBasicRookMoves(t *testing.T) {
	empty := NewBoard()
	rook := NewRook(White)
	withRook := empty.WithNewPiece(rook, 0, 0)

	moves := rook.AvailableMoves(withRook, 0, 0)

	if len(moves) != 2*7 { t.Errorf("14 moves expected, got %d", len(moves))}
}

func TestRookType(t *testing.T) {
	var p Piece = NewRook(White)
	if p.GetType() != RookType { t.Fail() }
}


func TestRookCoverage(t *testing.T) {
	empty := NewBoard()
	piece := NewRook(White)
	withPiece := empty.WithNewPiece(piece, 0, 0)

	coverage := piece.Coverage(withPiece, 0, 0)
	expected := 14

	if coverage != expected  { t.Errorf("Coverage expected to be %d, got %d", expected, coverage)}
}

func TestRookCoverageEmptyBoard(t *testing.T) {
	empty := NewBoard()
	piece := NewRook(White)
	withPiece := empty.WithNewPiece(piece, 0, 0)

	coverage := piece.Coverage(withPiece, 0, 0)
	expected := 14

	if coverage != expected  { t.Errorf("Coverage expected to be %d, got %d", expected, coverage)}
}

func TestRookCoverageSameColorShield(t *testing.T) {
	empty := NewBoard()
	piece := NewRook(White)
	withPiece := empty.WithNewPiece(piece, 0, 0)
	withPiece = withPiece.WithNewPiece(NewRook(White), 0 , 1) // shield

	coverage := piece.Coverage(withPiece, 0, 0)
	expected := 7

	if coverage != expected  { t.Errorf("Coverage expected to be %d, got %d", expected, coverage)}
}
