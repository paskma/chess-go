package piece

import (
	. "chess/common_types"
)

func PieceForSymbol(s string) Piece {
	switch s {
		case BishopSymbolWhite: return NewBishop(White)
		case BishopSymbolBlack: return NewBishop(Black)
		case KingSymbolWhite: return NewKing(White)
		case KingSymbolBlack: return NewKing(Black)
		case KnightSymbolWhite: return NewKnight(White)
		case KnightSymbolBlack: return NewKnight(Black)
		case PawnSymbolWhite: return NewPawn(White)
		case PawnSymbolBlack: return NewPawn(Black)
		case QueenSymbolWhite: return NewQueen(White)
		case QueenSymbolBlack: return NewQueen(Black)
		case RookSymbolWhite: return NewRook(White)
		case RookSymbolBlack: return NewRook(Black)
		default: return nil
	}
}
