package piece

import (
	. "chess/common_types"
	. "chess/move"
)

const (
	QueenSymbolWhite = "♕"
	QueenSymbolBlack = "♛"
)

type queen struct {
	AbstractPiece
}

func NewQueen(c Color) *queen {
	result := &queen{AbstractPiece{c, 10}}
	return result
}

func (q *queen)GetSymbol() string {
	if q.color == White {
		return QueenSymbolWhite
	} else {
		return QueenSymbolBlack
	}
}

func (q* queen)AvailableMoves(board BoardInfo, i, j int) []Move {
	visitor := NewMoveAdderVisitor()
	enumerateRookMoves(q, board, i, j, visitor)
	enumerateBishopMoves(q, board, i, j, visitor)
	return visitor.GetMoves()
}

func (q queen)AvailableStraitMoves(board BoardInfo, i, j int) []Move {
	return q.AvailableMoves(board, i, j)
}

func (q *queen)Coverage(board BoardInfo, i, j int) int {
	visitor := NewCoverageVisitor()
	enumerateRookMoves(q, board, i, j, visitor)
	enumerateBishopMoves(q, board, i, j, visitor)
	return visitor.Result()
}

func (q *queen)String() string {
	return q.formatString("Queen")
}
