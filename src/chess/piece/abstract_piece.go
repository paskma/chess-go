package piece

import (
	. "chess/common_types"
	"fmt"
)

type AbstractPiece struct {
	color Color
	weight int64
}

func (p *AbstractPiece) GetColor() Color {
	return p.color
}

func (p *AbstractPiece) IsWhite() bool {
	return p.color == White
}

func (p *AbstractPiece) IsBlack() bool {
	return p.color == Black
}


func (p *AbstractPiece) GetWeight() int64 {
	return p.weight
}

func (p *AbstractPiece) GetWeightScore() (result int64) {
	result = p.weight
	if p.color == Black {
		result = -p.weight
	}

	return result
}

func (p *AbstractPiece) GetType() int {
	return OtherType
}

func (p *AbstractPiece) formatString(name string) string {
		return fmt.Sprintf("%s(%s)", name, p.color.String())
}

// GetSymbol not implemented in AbstractPiece
// AvailableMoves not implemented in AbstractPiece
