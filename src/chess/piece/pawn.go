package piece

import (
	. "chess/common_types"
	. "chess/util"
	. "chess/move"
)

const (
	PawnSymbolWhite = "♙"
	PawnSymbolBlack = "♟"
)

type pawn struct {
	AbstractPiece
}


func NewPawn(c Color) *pawn {
	result := &pawn{AbstractPiece{c, 1}}
	return result
}

func (p *pawn)GetSymbol() string {
	if p.color == White {
		return PawnSymbolWhite
	} else {
		return PawnSymbolBlack
	}
}

func (p* pawn)AvailableMoves(board BoardInfo, i, j int) []Move {
	result := make([]Move, 0, 4)

	if p.IsWhite() {
		if IsValid(i + 1, j) && board.IsFree(i + 1, j) {
			result = append(result, NewSimpleMove(p, i, j, i + 1, j))
		}

		if i == 1 && board.AreFreeRows(Interval{i + 1, i + 2}, j) {
			result = append(result, NewSimpleMove(p, i, j, i + 2, j))
		}

		if IsValid(i + 1, j + 1) && board.IsBlackPiece(i + 1, j + 1) {
			result = append(result, NewSimpleMove(p, i, j, i + 1, j + 1))
		}


		if IsValid(i + 1, j -1) && board.IsBlackPiece(i + 1, j - 1) {
			result = append(result, NewSimpleMove(p, i, j, i + 1, j-1))
		}

		// todo: upgrade to a stronger piece when we reach the last row
	} else {
		if IsValid(i - 1, j) && board.IsFree(i - 1, j) {
			result = append(result, NewSimpleMove(p, i, j, i - 1, j))
		}

		if i == 6 && board.AreFreeRows(Interval{i - 2, i - 1}, j) {
			result = append(result, NewSimpleMove(p, i, j, i - 2, j))
		}

		if IsValid(i - 1, j + 1) && board.IsWhitePiece(i - 1, j + 1) {
			result = append(result, NewSimpleMove(p, i, j, i - 1, j + 1))
		}

		if IsValid(i - 1, j -1) && board.IsWhitePiece(i - 1, j - 1) {
			result = append(result, NewSimpleMove(p, i, j, i - 1, j-1))
		}

		// todo: promote to a stronger piece when we reach the first row
	}

	return result
}

func (p* pawn)AvailableStraitMoves(board BoardInfo, i, j int) []Move {
	return p.AvailableMoves(board, i, j)
}

func (p *pawn)String() string {
	return p.formatString("Pawn")
}

func (p *pawn)Coverage(board BoardInfo, i, j int) int {
	return 0
}
