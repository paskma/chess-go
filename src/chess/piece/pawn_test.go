package piece

import (
	"testing"
	. "chess/board"
	. "chess/common_types"
)


func TestTwoForwardPawnMoves(t *testing.T) {
	empty := NewBoard()
	pawn := NewPawn(White)
	withPawn := empty.WithNewPiece(pawn, 1, 1)

	moves := pawn.AvailableMoves(withPawn, 1, 1)

	if len(moves) != 2 { t.Errorf("Two moves expected, got %d", len(moves))}
}
