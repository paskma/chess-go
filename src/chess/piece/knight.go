package piece

import (
	. "chess/common_types"
	. "chess/move"
)

const (
	KnightSymbolWhite = "♘"
	KnightSymbolBlack = "♞"
)

type knight struct {
	AbstractPiece
}

func NewKnight(c Color) *knight {
	result := &knight{AbstractPiece{c, 3}}
	return result
}

func (k *knight)GetSymbol() string {
	if k.color == White {
		return KnightSymbolWhite
	} else {
		return KnightSymbolBlack
	}
}

type raw_move struct {
	row, col int
}

func (k* knight)AvailableMoves(board BoardInfo, row, col int) []Move {
	visitor := NewMoveAdderVisitor()
	k.enumerate(board, row, col, visitor)
	return visitor.GetMoves()
}

func (k *knight)Coverage(board BoardInfo, i, j int) int {
	visitor := NewCoverageVisitor()
	k.enumerate(board, i, j, visitor)
	return visitor.Result()
}

func (k* knight)enumerate(board BoardInfo, row, col int, visitor Visitor) {
	raw_moves := []raw_move {
		{row + 2, col + 1},
		{row + 2, col - 1},
		{row - 2, col + 1},
		{row - 2, col - 1},
		{row + 1, col + 2},
		{row + 1, col - 2},
		{row - 1, col + 2},
		{row - 1, col - 2}}

	exerciseMove := func (toRow, toCol int) {
		if IsValid(toRow, toCol) {
			visitor.Visit(k, row, col, toRow, toCol, board)
		}
	}

	for _, move := range raw_moves {
		exerciseMove(move.row, move.col)
	}
}

func (k* knight)AvailableStraitMoves(board BoardInfo, i, j int) []Move {
	return k.AvailableMoves(board, i, j)
}

func (k *knight)String() string {
	return k.formatString("Knight")
}
