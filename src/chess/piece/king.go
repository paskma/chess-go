package piece

import (
	. "chess/common_types"
	. "chess/move"
	. "chess/util"
)

const (
	KingSymbolWhite = "♔"
	KingSymbolBlack = "♚"
)

type king struct {
	AbstractPiece
}

func NewKing(c Color) *king {
	result := &king{AbstractPiece{c, 1000000000}}
	return result
}

func (k *king)GetSymbol() string {
	if k.color == White {
		return KingSymbolWhite
	} else {
		return KingSymbolBlack
	}
}

func (k* king)AvailableMoves(board BoardInfo, i, j int) []Move {
	result := make([]Move, 0, 10)
	result = append(result, k.AvailableStraitMoves(board, i, j)...)
	result = append(result, k.CastlingMoves(board, i, j)...)
	return result
}

func (k* king)AvailableStraitMoves(board BoardInfo, row, col int) []Move {
	result := make([]Move, 0, 8)

	for i := Max(row - 1, 0); i <= Min(row + 1, 7); i++ {
		for j := Max(col - 1, 0); j <= Min(col + 1, 7); j++ {
			AddMoveIfPossible(k, row, col, i, j, board, &result)
		}
	}

	return result
}

type lazyCoverage struct {
	value Coverage
	computed bool
	color Color
	board BoardInfo
}

func (lc *lazyCoverage) GetValue() *Coverage {
	if !lc.computed {
		lc.value = lc.board.BooleanCoverage(lc.color)
		lc.computed = true
	}

	return &lc.value
}

func anyCovered(coverage *Coverage, row int, cols Interval) bool  {
	for col := cols.First; col <= cols.Last; col++ {
		if coverage[row][col] {
			return true
		}
	}

	return false
}

func isRook(board  BoardInfo, row, col int, color Color) bool {
	p := board.GetPiece(row, col)
	if p == nil || p.GetType() != RookType {
		return false
	}

	return p.GetColor() == color
}

func (k *king)CastlingMoves(board BoardInfo, row, col int) []Move {
	result := make([]Move, 0, 2)
	coverage := lazyCoverage{color:k.GetColor().Other(), board:board}

	if k.IsWhite() && board.GetCastlingState().IsWhiteKingSideAvailable() &&
			board.AreFreeCols(ROW_1, Interval{COL_F, COL_G}) && isRook(board, ROW_1, COL_H, White) &&
			!anyCovered(coverage.GetValue(), ROW_1, Interval{COL_E, COL_G}) {
		result = append(result, NewCastlingMove(k, row, col, row, col + 2))
	}

	if k.IsWhite() && board.GetCastlingState().IsWhiteQueenSideAvailable() &&
			board.AreFreeCols(ROW_1, Interval{COL_B, COL_D}) && isRook(board, ROW_1, COL_A, White) &&
			!anyCovered(coverage.GetValue(), ROW_1, Interval{COL_C, COL_E}) {
		result = append(result, NewCastlingMove(k, row, col, row, col - 2))
	}

	if k.IsBlack() && board.GetCastlingState().IsBlackKingSideAvailable() &&
			board.AreFreeCols(ROW_8, Interval{COL_F, COL_G}) && isRook(board, ROW_8, COL_H, Black) &&
			!anyCovered(coverage.GetValue(), ROW_8, Interval{COL_E, COL_G}) {
		result = append(result, NewCastlingMove(k, row, col, row, col + 2))
}

	if k.IsBlack() && board.GetCastlingState().IsBlackQueenSideAvailable() &&
			board.AreFreeCols(ROW_8, Interval{COL_B, COL_D}) && isRook(board, ROW_8, COL_A, Black) &&
			!anyCovered(coverage.GetValue(), ROW_8, Interval{COL_C, COL_E}) {
		result = append(result, NewCastlingMove(k, row, col, row, col - 2))
	}
	
	return result
}

func (k *king)GetType() int {
	return KingType
}

func (k *king)String() string {
	return k.formatString("King")
}

func (k *king)Coverage(board BoardInfo, i, j int) int {
	return 0
}
