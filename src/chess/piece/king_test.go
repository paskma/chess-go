package piece


import (
	"testing"
	. "chess/board"
	. "chess/common_types"
)


func TestBasicKingMoves(t *testing.T) {
	empty := NewBoard()
	p := NewKing(White)
	withPiece := empty.WithNewPiece(p, 1, 1)

	moves := p.AvailableMoves(withPiece, 1, 1)

	const expected = 8
	if len(moves) != expected { t.Errorf("%d moves expected, got %d", expected, len(moves))}
}

func TestCastlingKingMoves(t *testing.T) {
	empty := NewBoard()
	p := NewKing(White)
	withPieces :=
		empty.WithNewPiece(p, ROW_1, COL_E).
			WithNewPiece(NewRook(White), ROW_1, COL_A).
			WithNewPiece(NewRook(White), ROW_1, COL_H)

	moves := p.AvailableMoves(withPieces, ROW_1, COL_E)

	const expected = 7
	if len(moves) != expected { t.Errorf("%d moves expected, got %d", expected, len(moves))}
}

func TestKingType(t *testing.T) {
	var p Piece = NewKnight(White)
	if p.GetType() != OtherType { t.Fail() }
}
