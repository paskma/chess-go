package piece

import (
	. "chess/common_types"
	. "chess/util"
	. "chess/move"
)

func enumerateBishopMoves(piece Piece, board BoardInfo, row, col int, visitor Visitor) {
	for x := 1; x <= Min(7 - row, 7 - col); x++ {
		if !visitor.Visit(piece, row, col, row + x, col + x, board) {
			break
		}
	}

	for x := 1; x <= Min(7 - row, col); x++ {
		if !visitor.Visit(piece, row, col, row + x, col - x, board) {
			break
		}
	}

	for x := 1; x <= Min(row, 7 - col); x++ {
		if !visitor.Visit(piece, row, col, row - x, col + x, board) {
			break
		}
	}

	for x := 1; x <= Min(row, col); x++ {
		if !visitor.Visit(piece, row, col, row - x, col - x, board) {
			break
		}
	}
}
