package piece

import (
	"testing"
	. "chess/board"
	. "chess/common_types"
)


func TestBasicKnightMovesNearCorner(t *testing.T) {
	empty := NewBoard()
	p := NewKnight(White)
	withPiece := empty.WithNewPiece(p, 1, 1)

	moves := p.AvailableMoves(withPiece, 1, 1)

	const expected = 4
	if len(moves) != expected { t.Errorf("%d moves expected, got %d", expected, len(moves))}
}

func TestBasicKnightMovesInTheMiddle(t *testing.T) {
	empty := NewBoard()
	p := NewKnight(White)
	withPiece := empty.WithNewPiece(p, 4, 4)

	moves := p.AvailableMoves(withPiece, 4, 4)

	const expected = 8
	if len(moves) != expected { t.Errorf("%d moves expected, got %d", expected, len(moves))}
}

func TestKnightCoverageInTheMiddle(t *testing.T) {
	empty := NewBoard()
	p := NewKnight(White)
	withPiece := empty.WithNewPiece(p, 4, 4)

	coverage := p.Coverage(withPiece, 4, 4)

	const expected = 8
	if coverage != expected { t.Errorf("%d coverage expected, got %d", expected, coverage)}
}

func TestKnightType(t *testing.T) {
	var p Piece = NewKnight(White)
	if p.GetType() != OtherType { t.Fail() }
}
