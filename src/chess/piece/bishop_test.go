package piece

import (
	"testing"
	. "chess/board"
	. "chess/common_types"
)


func TestBasicBishopMoves(t *testing.T) {
	empty := NewBoard()
	piece := NewBishop(White)
	withPiece := empty.WithNewPiece(piece, 1, 1)

	moves := piece.AvailableMoves(withPiece, 1, 1)

	if len(moves) != 6+1+1+1 { t.Errorf("9 moves expected, got %d", len(moves))}
}

func TestBishopCoverage(t *testing.T) {
	empty := NewBoard()
	piece := NewBishop(White)
	withPiece := empty.WithNewPiece(piece, 0, 0)

	coverage := piece.Coverage(withPiece, 0, 0)
	expected := 7

	if coverage != expected  { t.Errorf("Coverage expected to be %d, got %d", expected, coverage)}
}
