package piece

import (
	. "chess/common_types"
	. "chess/move"
)

const (
	BishopSymbolWhite = "♗"
	BishopSymbolBlack = "♝"
)

type bishop struct {
	AbstractPiece
}

func NewBishop(c Color) *bishop {
	result := &bishop{AbstractPiece{c, 3}}
	return result
}

func (b *bishop)GetSymbol() string {
	if b.color == White {
		return BishopSymbolWhite
	} else {
		return BishopSymbolBlack
	}
}

func (b* bishop)AvailableMoves(board BoardInfo, i, j int) []Move {
	visitor := NewMoveAdderVisitor()
	enumerateBishopMoves(b, board, i, j, visitor)
	return visitor.GetMoves()
}

func (b* bishop)Coverage(board BoardInfo, i, j int) int {
	visitor := NewCoverageVisitor()
	enumerateBishopMoves(b, board, i, j, visitor)
	return visitor.Result()
}

func (b* bishop)AvailableStraitMoves(board BoardInfo, i, j int) []Move {
	return b.AvailableMoves(board, i, j)
}

func (b *bishop)String() string {
	return b.formatString("Bishop")
}
