package piece

import (
	. "chess/common_types"
	. "chess/move"
)

func enumerateRookMoves(piece Piece, board BoardInfo, row, col int, visitor Visitor) {

	for x := row + 1; x <= 7; x++ {
		if !visitor.Visit(piece, row, col, x, col, board) {
			break
		}
	}

	for x := row - 1; x >= 0; x-- {
		if !visitor.Visit(piece, row, col, x, col, board) {
			break
		}
	}

	for x := col + 1; x <= 7; x++ {
		if !visitor.Visit(piece, row, col, row, x, board) {
			break
		}
	}

	for x := col - 1; x >= 0; x-- {
		if !visitor.Visit(piece, row, col, row, x, board) {
			break
		}
	}
}
