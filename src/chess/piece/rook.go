package piece

import (
	. "chess/common_types"
	. "chess/move"
)

const (
	RookSymbolWhite = "♖"
	RookSymbolBlack = "♜"
)

type rook struct {
	AbstractPiece
}

func NewRook(c Color) *rook {
	result := &rook{AbstractPiece{c, 5}}
	return result
}

func (r *rook)GetSymbol() string {
	if r.color == White {
		return RookSymbolWhite
	} else {
		return RookSymbolBlack
	}
}

func (r *rook)AvailableMoves(board BoardInfo, i, j int) []Move {
	visitor := NewMoveAdderVisitor()
	enumerateRookMoves(r, board, i, j, visitor)
	return visitor.GetMoves()
}

func (r *rook)Coverage(board BoardInfo, i, j int) int {
	visitor := NewCoverageVisitor()
	enumerateRookMoves(r, board, i, j, visitor)
	return visitor.Result()
}

func (r *rook)AvailableStraitMoves(board BoardInfo, i, j int) []Move {
	return r.AvailableMoves(board, i, j)
}

func (r *rook)GetType() int {
	return RookType
}

func (r *rook)String() string {
	return r.formatString("Rook")
}
