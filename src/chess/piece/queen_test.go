package piece

import (
	"testing"
	. "chess/board"
	. "chess/common_types"
)


func TestBasicQueenMoves(t *testing.T) {
	empty := NewBoard()
	piece := NewQueen(White)
	withPiece := empty.WithNewPiece(piece, 1, 1)

	moves := piece.AvailableMoves(withPiece, 1, 1)

	const expected = 6+1+1+1+2*7
	if len(moves) != expected { t.Errorf("%d moves expected, got %d", expected, len(moves))}
}

func TestQueenCoverageEmptyBoard(t *testing.T) {
	empty := NewBoard()
	piece := NewQueen(White)
	withPiece := empty.WithNewPiece(piece, 0, 0)

	coverage := piece.Coverage(withPiece, 0, 0)
	expected := 3*7

	if coverage != expected  { t.Errorf("Coverage expected to be %d, got %d", expected, coverage)}
}
