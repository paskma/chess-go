package util

func MinItem64(a []int64) int64 {
	result := a[0]
	for i := 1; i < len(a); i++ {
		result = Min64(result, a[i])
	}
	return result
}

func MaxItem64(a []int64) int64 {
	result := a[0]
	for i := 1; i < len(a); i++ {
		result = Max64(result, a[i])
	}
	return result
}
