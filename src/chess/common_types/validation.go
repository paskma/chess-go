package common_types

func IsValid(row, col int) bool {
	return row >= 0 && row < 8 && col >= 0 && col < 8
}
