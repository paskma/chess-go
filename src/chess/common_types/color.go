package common_types

const (
	White = 0
	Black = 1
)

type Color int

func (c Color)Other() Color {
	if c == White {
		return Black
	} else {
		return White
	}
}

func (c Color) String() string {
	if c == White {
		return "White"
	} else {
		return "Black"
	}
}
