package common_types

const (
	ROW_1 = 0
	ROW_2 = 1
	ROW_3 = 2
	ROW_4 = 3
	ROW_5 = 4
	ROW_6 = 5
	ROW_7 = 6
	ROW_8 = 7

	COL_A = 0
	COL_B = 1
	COL_C = 2
	COL_D = 3
	COL_E = 4
	COL_F = 5
	COL_G = 6
	COL_H = 7
)

/*
  TODO:
  def parseCoord(coord : String) : (Int, Int) = (coord.substring(1).toInt - 1, letterToColIndex(coord(0)))
 */

func ColIndexToLetter(col int) rune {
	return rune(int('a') + col)
}

func LetterToColIndex(letter rune) int {
	return int(letter) - int('a')
}
