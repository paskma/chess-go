package common_types

const (
	KingType = iota
	RookType = iota
	OtherType = iota
)

type Piece interface {
	GetColor() Color
	IsWhite() bool
	IsBlack() bool
	GetSymbol() string
	GetWeight() int64
	GetWeightScore() int64
	AvailableMoves(board BoardInfo, row, col int) []Move
	AvailableStraitMoves(board BoardInfo, row, col int) []Move
	GetType() int
	Coverage(board BoardInfo, row, col int) int
	String() string
}

