package common_types

import (
	"testing"
)


func TestColIndexToLetter(t *testing.T) {
	if ColIndexToLetter(1) != 'b' { t.Error("Second column is B.") }
}

func TestLetterToColIndex(t *testing.T) {
	if LetterToColIndex('b') != 1 { t.Error("B col has index 1.") }
}
