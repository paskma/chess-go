package common_types

import (
	. "chess/util"
)

type Fields [8][8] Piece
type Coverage [8][8] bool

type BoardInfo interface {
	IsFree(row, col int) bool
	AreFreeRows(rows Interval, col int) bool
	AreFreeCols(row int, cols Interval) bool
	IsWhitePiece(row, col int) bool
	IsBlackPiece(row, col int) bool
	IsPieceOfColor(row, col int, c Color) bool
	BooleanCoverage(color Color) Coverage
	GetPiece(roc, col int) Piece
	GetCastlingState() CastlingStateInfo
	String() string
	AvailableMoves(color Color) []Move
	WithMovedPiece(move Move) BoardInfo
	KingTaken() bool
	Score() int64
	WithCastling(castlingState CastlingStateInfo) BoardInfo
}

type CastlingStateInfo interface {
	IsWhiteKingSideAvailable() bool
	IsWhiteQueenSideAvailable() bool
	IsBlackKingSideAvailable() bool
	IsBlackQueenSideAvailable() bool
	AfterMoveOf(p Piece, sourceCol int) CastlingStateInfo
}

type Move interface {
	Execute(fields * Fields)
	GetPiece() Piece
	GetToRow() int
	GetToCol() int
	GetFromCol() int
	String() string
}
