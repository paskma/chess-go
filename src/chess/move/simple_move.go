package move

import (
	. "chess/common_types"
	"fmt"
)

type SimpleMove struct {
	piece Piece
	fromRow, fromCol, toRow, toCol int
}

func NewSimpleMove(piece Piece, fromRow, fromCol, toRow, toCol int) *SimpleMove {
	return &SimpleMove{piece, fromRow, fromCol, toRow, toCol}
}

func (m *SimpleMove) Execute(fields * Fields) {
	fields[m.toRow][m.toCol] = m.piece
	fields[m.fromRow][m.fromCol] = nil
}

func (m *SimpleMove) GetToRow() int {
	return m.toRow
}

func (m *SimpleMove) GetToCol() int {
	return m.toCol
}

func (m *SimpleMove) GetFromCol() int {
	return m.fromCol
}

func (m *SimpleMove) GetPiece() Piece {
	return m.piece
}

func (m *SimpleMove) String() string {
	return fmt.Sprintf("%s %s%d %s%d", m.piece.String(),
		string(ColIndexToLetter(m.fromCol)), m.fromRow + 1,
		string(ColIndexToLetter(m.toCol)), m.toRow + 1)
}
