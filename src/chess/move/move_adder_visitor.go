package move

import (
	. "chess/common_types"
)

type moveAdderVisitor struct {
	moves []Move
}

func NewMoveAdderVisitor() *moveAdderVisitor {
	return &moveAdderVisitor{moves : make([]Move, 0, 64)}
}

func (v *moveAdderVisitor)GetMoves() []Move {
	return v.moves
}

func (v *moveAdderVisitor) Visit(piece Piece, fromRow, fromCol, toRow, toCol int, board BoardInfo) bool {
	return AddMoveIfPossible(piece, fromRow, fromCol, toRow, toCol, board, &v.moves)
}
