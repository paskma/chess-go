package move

import (
	. "chess/common_types"
)

func AddMoveIfPossible(piece Piece,
	fromRow, fromCol, toRow, toCol int,
	board BoardInfo, result *[]Move) bool {

	if board.IsFree(toRow, toCol) {
		*result = append(*result, NewSimpleMove(piece, fromRow, fromCol, toRow, toCol))
		return true
	} else if board.IsPieceOfColor(toRow, toCol, piece.GetColor().Other()) {
		*result = append(*result, NewSimpleMove(piece, fromRow, fromCol, toRow, toCol))
		return false
	} else {
		return false
	}
}
