package move

import (
	. "chess/common_types"
)

type castlingMove struct {
	simple SimpleMove
}

func NewCastlingMove(piece Piece,
	fromRow, fromCol, toRow, toCol int) *castlingMove {

	return &castlingMove{SimpleMove{piece, fromRow, fromCol, toRow, toCol}}
}

func (m *castlingMove) Execute(fields * Fields) {
	m.simple.Execute(fields)

	if (m.simple.toCol > m.simple.fromCol) {
		// king side
		rook := fields[m.simple.toRow][7]
		fields[m.simple.toRow][m.simple.toCol - 1] = rook
		fields[m.simple.toRow][7] = nil
	} else {
		// queen side
		rook := fields[m.simple.toRow][0]
		fields[m.simple.toRow][m.simple.toCol + 1] = rook
		fields[m.simple.toRow][0] = nil
	}
}

func (m *castlingMove) GetToRow() int {
	return m.simple.toRow
}

func (m *castlingMove) GetToCol() int {
	return m.simple.toCol
}

func (m *castlingMove) GetFromCol() int {
	return m.simple.fromCol
}

func (m *castlingMove) GetPiece() Piece {
	return m.simple.GetPiece()
}

func (m *castlingMove) String() string {
	return m.simple.String()
}
