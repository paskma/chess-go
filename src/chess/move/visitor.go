package move

import (
	. "chess/common_types"
)

type Visitor interface {
	Visit(piece Piece, fromRow, fromCol, toRow, toCol int, board BoardInfo) bool
}
