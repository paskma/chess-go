package move

import (
	. "chess/common_types"
)

type coverageVisitor struct {
	result int
}

func NewCoverageVisitor() *coverageVisitor {
	return &coverageVisitor{result:0}
}

func (v *coverageVisitor)Result() int {
	return v.result
}

func (v *coverageVisitor) Visit(piece Piece, fromRow, fromCol, toRow, toCol int, board BoardInfo) bool {
	if board.IsFree(toRow, toCol) {
		v.result++
		return true
	} else if board.IsPieceOfColor(toRow, toCol, piece.GetColor().Other()) {
		v.result++
		return true
	} else {
		return false
	}
}
