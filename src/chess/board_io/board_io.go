package board_io

import (
	. "chess/common_types"
	. "chess/board"
	. "chess/piece"
)

func FromString(str string) BoardInfo {
	s := []rune(str)
	i := 0
	board := NewBoard()
	ignore := map[rune]bool {
		' ':true,
		'\r':true,
		'\n':true,
	}

	for row := 7; row >= 0; row-- {
		for col := 0; col <= 8; col++ {
			for {
				val, ok := ignore[s[i]]
				if ok && val {
					i++
				} else {
					break
				}
			}

			if col != 0 {
				piece := PieceForSymbol(string(s[i]))
				if piece != nil {
					board = board.WithNewPiece(piece, row, col-1)
				}
			}
			i++;
		}
	}

	return board
}

const InitialBoard = `8♜♞♝♛♚♝♞♜
7♟♟♟♟♟♟♟♟
6--------
5--------
4--------
3--------
2♙♙♙♙♙♙♙♙
1♖♘♗♕♔♗♘♖`
