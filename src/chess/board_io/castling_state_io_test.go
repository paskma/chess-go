package board_io


import (
	"testing"
	. "chess/board"
)


func TestCastlingStateIO(t *testing.T) {
	cs := NewCastlingState().WithDisabled(true, false, false, false)
	s := cs.String()
	cs2 := CastlingStateFromString(s)

	if cs.IsWhiteKingSideAvailable() != cs2.IsWhiteKingSideAvailable() { t.Fail() }
	if cs.IsWhiteQueenSideAvailable() != cs2.IsWhiteQueenSideAvailable() { t.Fail() }
}
