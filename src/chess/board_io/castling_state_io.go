package board_io

import (
	"strings"
	"strconv"
	. "chess/board"
)

func CastlingStateFromString(s string) *CastlingState {
	lines := strings.Split(s, "\n")

	whiteKing := true
	whiteQueen := true
	blackKing := true
	blackQueen := true

	for _, line := range lines {
		if strings.HasPrefix(line, Key_whiteKingSideCastling) {
			whiteKing = strings.HasSuffix(line, strconv.FormatBool(true))
		}
		if strings.HasPrefix(line, Key_whiteQueenSideCastling) {
			whiteQueen = strings.HasSuffix(line, strconv.FormatBool(true))
		}
		if strings.HasPrefix(line, Key_blackKingSideCastling) {
			blackKing = strings.HasSuffix(line, strconv.FormatBool(true))
		}
		if strings.HasPrefix(line, Key_blackQueenSideCastling) {
			blackQueen = strings.HasSuffix(line, strconv.FormatBool(true))
		}
	}

	return NewCastlingStateParams(whiteKing, whiteQueen, blackKing, blackQueen)
}
