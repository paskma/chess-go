package board_io

import (
	"testing"
)


func TestLoadFixedPoint(t *testing.T) {
	board := FromString(InitialBoard)
	actual := board.String()
	if actual != InitialBoard { t.Error(actual) }
}
